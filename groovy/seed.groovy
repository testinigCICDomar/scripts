job('buildjekllu') {


    steps {
        shell(readFileFromWorkspace('/home/gamefast/Desktop/scripts/shell/build.sh'))
    }

    publishers {
        //archive the war file generated
        archiveArtifacts('omar-site//_site/**.html')
        downstream 'testjekllu', 'SUCCESS'
    }

}

job('pushtogitlab'){
  environmentVariables(GITLAB_USER_LOGIN: 'testinigCICDomar', PRIVATE_TOKEN: 'xXNiiaf__eTWfE-h6Ge-')
  customWorkspace('/home/gamefast/Desktop/jenkins_home/workspace/buildjekllu/')
  steps {
    shell('cd omar-site/_site')
    shell('git add .')
    shell('git commit -m "creating site repo"')
    shell('git push -u origin master')
  }

}

job('testjekllu') {
    steps {

        copyArtifacts('buildjekllu') {
            targetDirectory('/target/')
            flatten()
            buildSelector {
                latestSuccessful(true)
            }
        }
        shell('grep -q "Welcome to Jekyll!" /target/index.html')
    }

     publishers {
	mailer('omarbn007@gmail.com', true, true)
        downstream 'servejekllu', 'SUCCESS'
    }

}

job('servejekllu') {
    customWorkspace('/home/gamefast/Desktop/jenkins_home/workspace/buildjekllu')

    steps {
        shell(readFileFromWorkspace('/home/gamefast/Desktop/scripts/shell/serve.sh'))
    }
	publishers {

       	 downstream 'testservejekllu', 'SUCCESS'
    }

}

job('testservejekllu') {
    steps {
        shell(readFileFromWorkspace('/home/gamefast/Desktop/scripts/shell/testservejekllu.sh'))
    }

    publishers {

         	 downstream 'surgejekllu', 'SUCCESS'
      }


}

job('surgejekllu') {
    customWorkspace('/home/gamefast/Desktop/jenkins_home/workspace/buildjekllu')
    environmentVariables(SURGE_LOGIN: 'omarbn007@gmail.com', SURGE_TOKEN: '1eef4e222e586e1820be252ba67aaad8')
    steps {
        shell(readFileFromWorkspace('/home/gamefast/Desktop/scripts/shell/serging.sh'))
    }
    publishers {

           downstream 'testsurgejekllu', 'SUCCESS'
      }
}





job('testsurgejekllu') {
    environmentVariables(SURGE_LOGIN: 'omarbn007@gmail.com', SURGE_TOKEN: '1eef4e222e586e1820be252ba67aaad8')
    steps {
        shell(readFileFromWorkspace('/home/gamefast/Desktop/scripts/shell/testserging.sh'))
    }
}

listView('gitlabconv') {
    jobs {
		name('buildjekllu')
		name('testjekllu')
		name('servejekllu')
		name('gitlab-seed')
		name('testservejekllu')
    name('surgejekllu')
    name('pushtogitlab')
    name('testsurgejekllu')
    }
    columns {
        status()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
        buildButton()
    }


}

deliveryPipelineView('job-dsl jekllu pipeline') {
    showAggregatedPipeline true
    enableManualTriggers true
    pipelineInstances 5
    pipelines {
        component('job-dsl jekllu pipeline', 'buildjekllu')
    }
}
